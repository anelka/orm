from orm import *
from Generator.classes import *
import unittest
from Generator.generator import *

class SetTests(unittest.TestCase):
    def setUp(self):
        self.category1 = Category(1)
        self.category2 = Category(2)
        self.category3 = Category(3)
        self.article1 = Article(1)
        self.article2 = Article(2)
        self.article3 = Article(3)
        self.user1 = User(email='elena@mail.ru')
        self.user2 = User(email='anelka.s@mail.ru')
        self.user3 = User(email='siava@pp.ua')
        self.tag1 = Tag(1)
        self.tag2 = Tag(2)
        self.tag3 = Tag(3)
        self.tag4 = Tag(4)

class SuccessTests(SetTests):
    def test_init1(self):
        category = Category()
        self.assertTrue(isinstance(category, Category))
    
    def test_init2(self):
        category = Category(1)
        self.assertTrue(isinstance(category, Category))
        with self.assertRaises(AttributeError):
            category = Category(id=1)
        
    def test_init3(self):
        category = Category(title='Category4')
        self.assertTrue(isinstance(category, Category))
        with self.assertRaises(AttributeError):
            category = Category(value='Category4')
    
    def test_init1(self):
        category = Category(2, title='Category4')
        self.assertTrue(isinstance(category, Category))
    
    def test_getattr1(self):
        self.assertEqual(self.category1.title, 'Category1')
        self.assertEqual(self.article3.author, 'elena')
    
    def test_getattr2(self):
        self.assertTrue(isinstance(self.article1.category, Category))
        self.assertEqual(self.article1.category.title, 'Category1')
    
    def test_getattr3(self):
        self.assertEqual(self.user1.category, self.category1)
        self.assertEqual(self.category2.user, self.user2)
    
    def test_getattr4(self):
        articles = [self.article1, self.article2]
        self.assertEqual(self.category1.articles, articles)
        tags = [self.tag1, self.tag2, self.tag3]
        self.assertEqual(self.article1.tags, tags)
        self.assertEqual(self.article3.tags, [])
    
    def test_getattr5(self):
        self.assertEqual(self.user3.category, None)
    
    def test_insert1(self):
        article = Article(title='Article4', author='siava', category=self.category2)
        article.save()
        self.assertEqual(article.category.title, 'Category2')
        article.delete()
    
    def test_insert2(self):
        with self.assertRaises(AttributeError):
            article = Article(title='Article4', category=self.category2)
            article.save()
    
    def test_insert3(self):
        with self.assertRaises(AttributeError):
            article = Article(title='Article4', category=self.tag2)
            article.save()
    
    def test_insert4(self):
        with self.assertRaises(RelationSetError):
            user = User(login='test', email='test@test',
                        password='test', category=self.category1)
            user.save()
    
    def test_insert5(self):
        with self.assertRaises(RelationSetError):
            category = Category(title='Category4', user=self.user1)
            category.save()
    
    def test_insert6(self):
        category = Category(title='Category4', user=self.user3)
        category.save()
        self.assertEqual(self.user3.category, category)
        category.delete()
    
    def test_insert7(self):
        category = Category(title='Category4')
        category.save()
        category.delete()
    
    def test_update1(self):
        article = Article(1)
        self.article1.title = 'Test'
        self.article1.save()
        self.assertEqual(article.title, 'Test')
        self.article1.title = 'Article1'
        self.article1.save()
        
    def test_update2(self):
        self.user1.category = None
        self.user1.save()
        self.assertEqual(self.category1.user, None)
        self.category1.user = self.user1
        self.category1.save()
        self.assertEqual(self.category1.user.login, 'elena')
        self.assertEqual(self.user1.category, self.category1)
    
    def test_update3(self):
        with self.assertRaises(RelationSetError):
            self.user1.category = self.article1
            self.user1.save()
    
    def test_update4(self):
        with self.assertRaises(RelationSetError):
            self.user1.category = self.category2
            self.user1.save()
    
    def test_add_relation1(self):
        self.article1.add_relation(self.tag4)
        self.assertTrue(self.article1 in self.tag4.articles)
        self.article1.remove_relation(self.tag4)
        self.assertFalse(self.article1 in self.tag4.articles)
    
    def test_add_relation2(self):
        with self.assertRaises(RelationSetError):
            self.article1.add_relation(self.category2)
    
    def test_add_relation3(self):
        with self.assertRaises(RelationSetError):
            self.category1.add_relation(self.article2)
    
    def test_remove_relation1(self):
        self.tag3.remove_relation(self.article1)
        self.assertFalse(self.tag3 in self.article1.tags)
        self.tag3.add_relation(self.article1)
        self.assertTrue(self.tag3 in self.article1.tags)
    
    def test_remove_relation2(self):
        with self.assertRaises(RelationSetError):
            self.article1.remove_relation(self.category2)
    
    def test_remove_relation3(self):
        with self.assertRaises(RelationSetError):
            self.category1.remove_relation(self.article2)
        
if __name__ == '__main__':
    #unittest.main()
    
    generator = Generator()
    generator.load('Generator/schema.yaml')
    generator.create_classes()
    generator.dump('Generator/classes.py')
    
    Category.bulk_delete(Category.filter(id__greater=10))

    if Connection.conn:
        Connection.cursor.close()
        Connection.conn.close()
