import psycopg2
import psycopg2.extras

class Connection(object):
    try:
        conn = psycopg2.connect(
            database="orm", user="orm",
            password='12345', host="localhost"
            )
    except psycopg2.DatabaseError as e:
        print 'Error %s' % e    
        sys.exit(1)
    conn.autocommit = True
    cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        
    @classmethod
    def get_cursor(cls):
        return cls.cursor
