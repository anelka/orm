import psycopg2
import psycopg2.extras
import sys
import yaml

from connection import Connection
from exceptions import *
from Generator.constant import Constant

class DatabaseError(Exception): pass
class DeleteError(Exception): pass
class FilterError(Exception): pass
class RelationSetError(Exception): pass

class Entity(object):
    _fields = _pk = _parents = _children = _siblings = _extensible = _extension = []
    _cursor = Connection.get_cursor()
    
    def __init__(self, ident=None, **kwargs):
        self.table_name = self.__class__.__name__.lower()
        self.pk = self._pk == 'id' and ident or None
        
        for field, value in kwargs.items():
            setattr(self, field, value)
            if (self._pk == field and self.__execute(Constant.IS_EXIST %
                                        {'table_name': self.table_name,
                                         'pk_field': field,
                                         'pk': value}, [])):
                self.pk = value
    
    def __execute(self, statement, args):
        try:
            self._cursor.execute(statement, args)
            try:
                return self._cursor.fetchone()
            except psycopg2.Error:
                pass
        except psycopg2.DatabaseError as e:
            raise  DatabaseError(error)
    
    def __setattr__(self, attr, value):
        if attr in (self._fields + self._parents + self._extensible +
                    self._extension + self._children + self._siblings +
                    ['table_name', 'pk']):
            super(Entity, self).__setattr__(attr, value)
        else:
            raise AttributeError('The instance %s does not have attribute %s'
                                                    % (self.table_name, attr))
    
    def __getattr__(self, attr):
        try:
            return super(Entity, self).__getattr__(attr)
        except AttributeError:
            args = {'table_name1': self.table_name,
                    'table_name2': self.table_name,
                    'pk_field': 'pk',
                    'pk': self.pk}
            foreign_keys = self._parents + self._extensible
            
            if attr in self._fields + foreign_keys:
                args['pk_field'] = self._pk
                val = self.__execute(Constant.SELECT % args, [])
                
                for field in self._fields:
                    setattr(self, field, val['%s_%s' % (self.table_name, field)])
                    
                for field in foreign_keys:
                    if val['%s_pk' % field]:
                        instance = getattr(sys.modules['Generator.classes'],
                                            field.capitalize())()
                        instance.pk = val['%s_pk' % field]
                    else:
                        instance = None
                    setattr(self, field, instance)
                return getattr(self, attr)
            elif attr in self._extension:
                args['table_name1'] = attr
                val = self.__execute(Constant.SELECT % args, [])
                if val:
                    instance = getattr(sys.modules['Generator.classes'], attr.capitalize())()
                    instance.pk = val['%s_%s' % (attr, instance._pk)]
                    setattr(instance, self.table_name, self)
                else:
                    instance = None
                setattr(self, attr, instance)
                return instance
            elif attr in self._children + self._siblings:
                relation_name = attr[:-1]
                relation_class = getattr(sys.modules['Generator.classes'],
                                        relation_name.capitalize())
                
                if attr in self._children:
                    args['table_name1'] = relation_name
                    relation_pk = relation_class._pk
                else:
                    args['table_name1'] = '%s_%s' % tuple(sorted([self.table_name, relation_name]))
                    relation_pk = 'pk'
                self._cursor.execute(Constant.SELECT % args)
                return [relation_class(item['%s_%s' % (relation_name, relation_pk)])
                            for item in self._cursor.fetchall()]
                
            raise AttributeError, 'Instance %s has no attribute %s' % (self.table_name, attr)
    
    def __collect_attr(self):
        values = []
        for field in self._fields:
            try:
                values.append('\'%s\'' % self.__dict__[field])
            except KeyError:
                values.append(None)
        for field in self._parents + self._extensible:
            try:
                instance = self.__dict__[field]
                ref_class = getattr(sys.modules['Generator.classes'], field.capitalize())
                if isinstance(instance, ref_class):
                    if field in self._extensible and getattr(instance, self.table_name):
                        raise RelationSetError('The instance %s already has %s.' %
                                    (field, self.table_name))
                    values.append(instance.pk)
                elif instance == None:
                    values.append(None)
                else:
                    raise RelationSetError('The %s must be the instance of class %s' %
                            (field, ref_class))    
            except KeyError:
                    values.append(None)
        return values
    
    def __insert(self):
        if all(field in self.__dict__ and field != None for field in self._required_fields):
            fields = ['%s_%s' % (self.table_name, field) for field in self._fields]
            parents = ['%s_pk' % field for field in self._parents + self._extensible]
            extensions = []
            values = self.__collect_attr()
            
            for field in self._extension:
                try:
                    instance = self.__dict__[field]
                    ref_class = getattr(sys.modules['Generator.classes'], field.capitalize())
                    if isinstance(instance, ref_class):
                        if getattr(instance, self.table_name):
                            raise RelationSetError('The instance %s already has %s.' %
                                        (field, self.table_name))
                        extensions.append(instance)
                    else:
                        raise RelationSetError('The %s must be the instance of class %s' %
                                (field, ref_class))    
                except KeyError: pass
                    
            args = {'table_name' : self.table_name,
                    'fields' : ', '.join(fields + parents),
                    'val' : ', '.join(['%s']*len(fields + parents)),
                    'pk_field': self._pk}
                    
            if any(field for field in values):
                val = self.__execute(Constant.INSERT % args, values)
                setattr(self, 'pk', val['%s_%s' % (self.table_name, self._pk)])
                for instance in extensions:
                    setattr(instance, self.table_name, self)
                    instance.save()
            else:
                print 'Instance was not created because all fields are Null.'
            
        else:
            raise AttributeError, 'Fields %s are required.' % ', '.join(self._required_fields)
        
        
    def __update(self):
        fields = ['%s_%s' % (self.table_name, field) for field in self._fields
                    if field in self.__dict__.keys()]
        parents = ['%s_pk' % field for field in self._parents
                    if field in self.__dict__.keys()]
        extensibles = ['%s_pk' % field for field in self._extensible
                    if field in self.__dict__.keys()]
        extensions = []
        values = []
        
        for field in self._fields:
            try:
                values.append(self.__dict__[field])
            except KeyError: pass
        for field in self._parents + self._extensible + self._extension:
            try:
                instance = self.__dict__[field]
                ref_class = getattr(sys.modules['Generator.classes'], field.capitalize())
                if isinstance(instance, ref_class):
                    if field in self._parents:
                        values.append(instance.pk)
                    elif getattr(instance, self.table_name) not in (self, None):
                        raise RelationSetError('The instance %s already has %s' %
                                    (field, self.table_name))
                    elif field in self._extensible:
                        setattr(instance, self.table_name, self)
                        values.append(instance.pk)
                    else:
                        extensions.append(instance)
                elif instance == None:
                    values.append(None)
                else:
                    raise RelationSetError('The %s must be the instance of class %s' %
                                (field, ref_class))
            except KeyError: pass
                
        args = {'table_name' : self.table_name,
                'fields' : ', '.join(fields + parents + extensibles),
                'val' : ', '.join(['%s']*len(fields + parents + extensibles)),
                'pk_field': self._pk,
                'pk' : self.pk}
        if any(field for field in values):
            val = self.__execute(Constant.UPDATE % args, values)
        for instance in extensions:
            setattr(instance, self.table_name, self)
            instance.save()
    
    def save(self):
        if self.pk:
            self.__update()
        else:
            self.__insert()
    
    def manage_relations(self, sibling, statement):
        relation_name = sibling.__class__.__name__.lower()
        if '%ss' % relation_name in self._siblings:
            self.__execute(statement % {
                'con_table': '%s_%s' % tuple(sorted([self.table_name, relation_name])),
                'table_name1': self.table_name,
                'table_name2': relation_name,
                'pk1': self.pk,
                'pk2': sibling.pk}, [])
        else:
            raise RelationSetError('The %s must be the instance of class %s' %
                                (sibling.__class__, self.__class__)) 
    
    def add_relation(self, sibling):
        self.manage_relations(sibling, Constant.INSERT_RELATION)
    
    def remove_relation(self, sibling):
        self.manage_relations(sibling, Constant.DELETE_RELATION)
            
    def delete(self):
        if self.pk:
            args = {'table_name' : self.table_name,
                    'pk_field': self._pk,
                    'pk': self.pk}
            self.__execute(Constant.DELETE % args, [])
        else:
            raise RuntimeError('This instance does not exist')
    
    def prefetch_related(self, relations):
        relations = relations.split('__')
        prev = self.__class__
        prev_dict = {self.pk: self}
        
        for next in relations:
            prev_tn = prev.__name__.lower()
            next_tn = next in prev._children + prev._siblings and next[:-1] or next
            next_class = getattr(sys.modules['Generator.classes'], next_tn.capitalize())
            next_dict = {}
            args = {'table_name1': prev_tn,
                    'table_name2': prev_tn,
                    'pk_field': 'pk',
                    'pk_list': ', '.join(str(pk) for pk in prev_dict.keys())}
            
            if next in prev._parents + prev._extensible:
                args['joins'] = Constant.JOIN % {'join_table': next_tn,
                                                'table': next_tn,
                                                'field1': prev._pk,
                                                'field2': 'pk'}
                args['pk_field'] = prev._pk 
                prev._cursor.execute(Constant.SELECT_PR % args)
                for row in prev._cursor.fetchall():
                    instance = next_class()
                    instance.pk = row['%s_pk' % next_tn]
                    for field in instance._fields:
                        setattr(instance, field, row['%s_%s' % (next_tn, field)])
                    setattr(prev_dict[row['%s_%s' % (prev_tn, prev._pk)]], next, instance)
                    next_dict[instance.pk] = instance
                    
            elif next in prev._children + prev._extension:
                attr = dict((pk, set()) for pk in prev_dict.keys())
                args['table_name1'] = next_tn
                args['joins'] = ''
                
                prev._cursor.execute(Constant.SELECT_PR % args)
                for row in prev._cursor.fetchall():
                    instance = next_class()
                    instance.pk = row['%s_%s' % (next_tn, next_class._pk)]
                    for field in instance._fields:
                        setattr(instance, field, row['%s_%s' % (next_tn, field)])
                    setattr(instance, prev_tn, prev_dict[row['%s_pk' % prev_tn]])
                    attr[row['%s_pk' % prev_tn]].add(instance)
                    next_dict[instance.pk] = instance
                for key, instance in prev_dict.items():
                    setattr(instance, next, attr[key])
            elif next in prev._siblings:
                attr = dict((pk, set()) for pk in prev_dict.keys())
                args['joins'] = Constant.JOIN % {'join_table': '%s_%s'
                                                        % tuple(sorted([next_tn, prev_tn])),
                                            'table': next_tn,
                                            'field1': '%s_%s' % (next_tn, prev._pk),
                                            'field2': '%s_pk' % next_tn}
                args['table_name1'] = next_tn
                
                prev._cursor.execute(Constant.SELECT_PR % args)
                for row in prev._cursor.fetchall():
                    instance = next_class()
                    instance.pk = row['%s_pk' % next_tn]
                    for field in instance._fields:
                        setattr(instance, field, row['%s_%s' % (next_tn, field)])
                    attr[row['%s_pk' % prev_tn]].add(instance)
                    next_dict[instance.pk] = instance
                for key, instance in prev_dict.items():
                    setattr(instance, next, attr[key])
            else:
                raise RelationSetError('Invalid connections given')
            prev = next_class
            prev_dict = next_dict

    def __eq__(self, other):
        if self.__class__ == other.__class__:
            return self.pk == other.pk
        else:
            return False
    
    def __ne__(self,other):
        return not self.__eq__(other)
    
    @classmethod
    def all(cls):
        table_name = cls.__name__.lower()
        cls._cursor.execute(Constant.SELECT_ALL % {'table_name': table_name,
                                                   'pk_field': cls._pk})
        for ident in cls._cursor.fetchall():
            instance = cls()
            setattr(instance, 'pk', ident['%s_%s' % (table_name, cls._pk)])
            yield instance
    
    @classmethod
    def objects_list(cls):
        return list(cls.all())
    
    @classmethod
    def count(cls):
        cls._cursor.execute(Constant.COUNT_ALL % {'table_name': cls.__name__.lower()})
        return cls._cursor.fetchone()['count']
    
    @classmethod
    def filter(cls, **kwargs):
        table_name = cls.__name__.lower()
        conditions = []
        for key, value in kwargs.items():
            if key in cls._fields:
                conditions.append('%s_%s=\'%s\'' % (table_name, key, value))
            elif '__' in key:
                attr, condition = key.split('__', 1) 
                if attr not in cls._fields + [cls._pk]:
                    raise FilterError('%s is not an attribute of class %s' %
                                                        (attr, table_name))
                try:
                    conditions.append(Constant.FILTER_CONDITIONS[condition] %
                                                        {'table': table_name,
                                                        'field': attr,
                                                        'filter': value,
                                                        'per': '%'})
                except KeyError:
                    raise FilterError('Invalid filter condition %s' % condition)
        
        if conditions:
            cls._cursor.execute(Constant.FILTER % {'table_name': table_name,
                                                'pk_field': cls._pk,
                                                'condition': ' AND '.join(conditions)})
            result = []
            for ident in cls._cursor.fetchall():
                instance = cls()
                instance.pk = ident['%s_%s' % (table_name, cls._pk)]
                result.append(instance)
            return result
        else:
            raise FilterError('You must add some filter condition.')
    
    @classmethod
    def bulk_create(cls, *args):
        if not all(isinstance(obj, cls) for obj in args):
            raise ValueError('All instances must be of class %s' % cls.__name__)
        if not all(all(field in obj.__dict__ and field != None
                        for field in obj._required_fields) for obj in args):
            raise AttributeError('Fields %s are required.' % ', '.join(cls._required_fields))
        
        table_name = cls.__name__.lower()
        fields = ['%s_%s' % (table_name, field) for field in cls._fields]
        parents = ['%s_pk' % field for field in cls._parents + cls._extensible]
        values = '), ('.join([', '.join(obj.__collect_attr()) for obj in args])
        arg = {'table_name' : table_name,
                'fields' : ', '.join(fields + parents),
                'val' : ', '.join(['%s']*len(fields + parents)),
                'pk_field': cls._pk}
        
        try:
            cls._cursor.execute(Constant.INSERT % arg % values)
        except psycopg2.DatabaseError as error:
            raise  DatabaseError(error)
            
        for obj, val in zip(args, cls._cursor.fetchall()):
            setattr(obj, 'pk', val['%s_%s' % (obj.table_name, obj._pk)])
    
    @classmethod
    def bulk_update(cls, instances, **kwargs):
        if not all(isinstance(instance, cls) for instance in instances):
            raise ValueError('All instances must be of class %s' % cls.__name__)
        if not set(kwargs.keys()).issubset(cls._fields + cls._parents):
            raise AttributeError('Unexpected attribute')
        
        table_name = cls.__name__.lower()
        values = []
        for field, value in kwargs.items():
            if field in cls._fields:
                values.append('\'%s\'' % value)
            else:
                parent_class = getattr(sys.modules['Generator.classes'], field.capitalize())
                if isinstance(value, parent_class):
                    values.append(str(value.pk))
                elif value == None:
                    values.append(None)
                else:
                    raiseAttributeError('Unexpected attribute')
        try:
            cls._cursor.execute(Constant.BULK_UPDATE %  {'table_name': table_name,
                'fields': ', '.join(['%s_%s' % (table_name, key) for key in kwargs.keys()]),
                'val': ', '.join(values),
                'pk_field': cls._pk,
                'pk_list': ', '.join([str(obj.pk) for obj in instances])})
        except psycopg2.DatabaseError as error:
            raise  DatabaseError(error)
            
    @classmethod
    def bulk_delete(cls, args):
        if len(args) > 0:
            if not all(isinstance(obj, cls) for obj in args):
                raise ValueError('All instances must be of class %s' % cls.__name__)
            
            try:
                cls._cursor.execute(Constant.BULK_DELETE % {'table_name': cls.__name__.lower(),
                                            'pk_field': cls._pk,
                                            'pk_list': ', '.join([str(obj.pk) for obj in args])})
            except psycopg2.DatabaseError as error:
                raise  DatabaseError(error)
