__all__ = (
    'RelationSetError',
    'FilterError',
    'DeleteError',
    )

class RelationSetError(Exception): pass
class FilterError(Exception): pass
class DeleteError(Exception): pass
